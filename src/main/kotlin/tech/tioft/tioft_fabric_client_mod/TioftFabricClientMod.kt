package tech.tioft.tioft_fabric_client_mod

import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.arguments.IntegerArgumentType
import com.mojang.brigadier.arguments.StringArgumentType
import com.mojang.brigadier.context.CommandContext
import io.github.cottonmc.clientcommands.ArgumentBuilders
import io.github.cottonmc.clientcommands.ClientCommandPlugin
import io.github.cottonmc.clientcommands.CottonClientCommandSource
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents
import net.minecraft.client.MinecraftClient
import net.minecraft.client.options.KeyBinding
import net.minecraft.client.util.InputUtil
import net.minecraft.command.argument.BlockPredicateArgumentType
import net.minecraft.server.command.CommandManager
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.text.LiteralText
import org.lwjgl.glfw.GLFW


class TioftFabricClientMod : ClientModInitializer, ClientCommandPlugin {
    val modid = "tioft_fabric_client_mod"
    var keybinding: KeyBinding? = null
    override fun onInitializeClient() {
        keybinding = KeyBinding(
            "key.tioft_fabric_client_mod.scream",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_U,
            "catagory.tioft_fabric_client_mod.utils"
        )

        registerClientTickCallback()
    }

    private fun registerClientTickCallback() {
        ClientTickEvents.END_CLIENT_TICK.register(ClientTickEvents.EndTick { client: MinecraftClient ->
            while (keybinding?.wasPressed() == true) {
                client.player!!.sendMessage(LiteralText("Key U was pressed!"), false)
            }
        })
    }

    override fun registerCommands(dispatcher: CommandDispatcher<CottonClientCommandSource>?) {
        dispatcher?.register(ArgumentBuilders
            .literal("build-cube")
            .then(
                ArgumentBuilders.argument("width", IntegerArgumentType.integer())
                    .then(
                        ArgumentBuilders.argument("length", IntegerArgumentType.integer())
                            .then(
                                ArgumentBuilders.argument("height", IntegerArgumentType.integer())
                                    .then(
                                        ArgumentBuilders.argument(
                                            "block_type",
                                            StringArgumentType.string()
                                        )
                                            .executes {
                                                val width = IntegerArgumentType.getInteger(it, "width")
                                                val length = IntegerArgumentType.getInteger(it, "length")
                                                val height = IntegerArgumentType.getInteger(it, "height")
                                                var blockType = StringArgumentType.getString(it, "block_type")
                                                it?.source?.sendFeedback(
                                                    LiteralText(
                                                        """
                                                        Building Cube with 
                                                        width = $width
                                                        length = $length
                                                        height = $height
                                                        block_type = ${blockType.toString()}
                                                        """.trimIndent()
                                                    )
                                                )
                                                1
                                            }
                                    )
                            )
                    )
            )
            .executes { source ->
                source?.source?.sendFeedback(LiteralText("Building Cube requires width, length, height, and block type"))
                1
            })
    }

}
